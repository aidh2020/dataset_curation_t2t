#!/usr/bin/python3

import os
import subprocess
import zipfile
from pathlib import Path

# PATHS
# Put extracted dataset here
PATH_TO_IMAGES = Path("data/image_dir")
# Leave as is
PATH_TO_JSON_SAVE = Path("data/json/")


def run_openpose(image_dir):

    # copies subdirectory structure in order to save json in the same way
    tmp = Path(*image_dir.parts[2:])

    # create json directories
    json_save = PATH_TO_JSON_SAVE / tmp
    if not json_save.is_dir():    
        string = str(json_save)    
        os.makedirs(string)

    # calls openpose
    subprocess.run(
        ['sh', './openpose_run_on_image_dir.sh', str(image_dir)+'/', str(json_save)])


# removes spaces for directories and files
# because OpenPose does not support them
def remove(string_input):
    return "_".join(string_input.split())


def main():

    # checks for png files in path './data/image_dir'
    for path in PATH_TO_IMAGES.glob(r'*'):

        for directory in path.glob(r'*'):

            # removes spaces from all directories and video names
            # and renames the files/directories
            new_name = remove(str(directory))
            directory.rename(new_name)

            run_openpose(directory)


if __name__ == '__main__':
    main()
