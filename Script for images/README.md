# HOW TO USE

1. First prepare the Dataset in the following structure (path with **bold** must be created):

    - openpose/**data/image_dir/-PLACE DATASET HERE-** <br/>
        - Dataset must have the following structure
            - image_dir
                - dataset
                    - file containing images
                        - images
                        <br />
    - openpose/**data/json**    
    <br />

2. Place both files in Openpose's directory (openpose/)

3. Run python file 
