# Dataset Curation T2T


## Datasets
We have extracted the key-points using the BODY_25 + hands + face OpenPose models for the News and Elementary Greek Sign Language Datasets.
The datasets are available here: https://zenodo.org/record/5903287




## Instructions on using the script for automating OpenPose key-point extraction

### Instructions for docker
Pull the openpose docker and build it:
- docker pull exsidius/openpose
- sudo docker run -it --name openpose --gpus all exsidius/openpose -v /directory_on_host:/openpose/data/

### Instructions for using the script 'openpose_run_on_video_dir'.
#### Script consists of two files:
- 'openpose_run_on_video_dir.sh', and 
- 'openpose_script_run_on_video_dir.py'

##### 1) 'openpose_run_on_video.sh'
This file contains the command that will launch OpenPose. By default it has the following arguements:
* --video *path_to_video*
* --write_json *path_to_save_json*
* --face 
* --hand
* --render_pose *0*

As well as the following two arguements, that specify which GPU to select in a multi-GPU setup.
* --num_gpu_start 0 
* --num_gpu 1 

More details on OpenPose commands can be found on their official website: https://cmu-perceptual-computing-lab.github.io/

##### 2) 'openpose_scripy_run_on_video_dir.py'
The two files must be in the installed directory of OpenPose. <br>
The script will look for a folder called **data** in the same directory. This can be changed by changing the value of *PATH_TO_DATASET* inside the file.


