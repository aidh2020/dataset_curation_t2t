#!/usr/bin/python3

import os
import subprocess
from pathlib import Path

# PATHS
# Put extracted dataset here
PATH_TO_DATASET = Path("./data/dataset/")
# Leave as is
PATH_TO_JSON_SAVE = Path("./data/json/")


def run_openpose(video_dir):

    # copies subdirectory structure in order to save json in the same way
    tmp = Path(*video_dir.parts[1:])
    # create json directories
    json_save = PATH_TO_JSON_SAVE / tmp
    if not json_save.is_dir():
        string = str(json_save)
        os.makedirs(string)
        # calls openpose
        subprocess.run(
        ['sh', './openpose_run_on_video_dir.sh', str(video_dir), str(json_save)])
    else:
        print("File already processed, skipping. ")


# removes spaces from directories and files
def remove(string_input):
    string_input = str(string_input)
    new_string = "_".join(string_input.split())
    return Path(new_string)


def main():

    # extensions to search for
    EXTENSIONS = ['.mov', '.mp4', '.flv', '.MOV']

    for path in PATH_TO_DATASET.glob('**/*'):
        new_path = remove(path)
        os.rename(str(path), str(new_path))
        
        if new_path.suffix in EXTENSIONS:

            # runs openpose
            os.system('clear')
            print("Now working on file: ", str(new_path.parts[2:]))
            
            run_openpose(new_path)
            
            
if __name__ == '__main__':
    main()
